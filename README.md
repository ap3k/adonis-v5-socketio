# AdonisJS V5 SocketIO example

This project is example project using AdonisJS V5 with SocketIO

Interesting parts are `app/Services/Ws.ts`, `providers/AppProvider.ts` and `start/routes.ts`

Or to see diff how it was added check out this commit: https://gitlab.com/ap3k/adonis-v5-socketio/-/commit/063f721726e7a40727a3004632202c571ce5c31a
